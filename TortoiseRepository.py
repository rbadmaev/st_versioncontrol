import sublime, os, glob, subprocess
from sublime_plugin import WindowCommand

class TortoiseRepositoryCommand(WindowCommand):
    _PathPlaceholder = "$File"

    def __init__(Self, Window, RepositoryPath = ""):
        super().__init__(Window)
        Self.RepositoryPath = RepositoryPath
        Self._Command = ""
        Self._Paths = []
        Self._Commands = []
        Self._NeedPath = False

    def _ExecuteForSelectedPath(Self, Index):
        if Index == -1:
            return

        command = Self._Command
        if Self._NeedPath:
            command = [v.replace(Self._PathPlaceholder, Self._Paths[Index]) for v in command]

        subprocess.Popen(command)

    def _Execute(Self, Command, current_file = False, repository = False, AskConfirmation = []):
        if repository and current_file:
            sublime.error_message("It can't be chosen both current_file and repository options")
            return

        if AskConfirmation:
            if len(AskConfirmation) == 1:
                if not sublime.ok_cancel_dialog(AskConfirmation[0]):
                    return
            elif len(AskConfirmation) == 2:
                if not sublime.ok_cancel_dialog(AskConfirmation[0], AskConfirmation[1]):
                    return
            else:
                sublime.error_message("invalid AskConfirmation param")
                return

        Self._NeedPath = False;
        for param in Command:
            if param.find(Self._PathPlaceholder) != -1:
                Self._NeedPath = True;
                break

        if Self._NeedPath:
            path = Self.window.active_view().file_name()
            while True:
                newpath = os.path.dirname(path)
                if newpath == path:
                    break
                if not repository or path == Self.RepositoryPath:
                    Self._Paths += [path]
                if current_file:
                    break;
                if path == Self.RepositoryPath:
                    break;
                path = newpath

            if len(Self._Paths) == 0:
                sublime.error_message("target path not found")
                return

        Self._Command = Command

        if len(Self._Paths) == 1 or not Self._NeedPath:
            Self._ExecuteForSelectedPath(0)
            return

        paths = [os.path.basename(v) for v in Self._Paths]
        Self.window.show_quick_panel(paths, Self._ExecuteForSelectedPath)

    def _onCommandChoosen(Self, Index):
        if Index == -1:
            return
        Self._Execute(  Self._Commands[Index][1],
                        Self._Commands[Index][2],
                        Self._Commands[Index][3],
                        Self._Commands[Index][4])

    def Name(Self):
        return "NEED TO OVERRIDE Name() METHOD"

    def BuildCommands(Self):
        # [[  Name,
        #     [Command, param1, param2...],
        #     bool (Use current file as path),
        #     bool (Use repository root as path),
        #     ["Ask for confirmation", "Ok button caption"]]]
        return [["SVN: Show log...", ["TortoiseProc", "/command:log", "/path:" + Self._PathPlaceholder], False, False, ["Do you want to continue?", "Yes"]]]

    def run(Self, RepositoryPath = "", TortoiseProc = ""):
        if RepositoryPath:
            Self.RepositoryPath = RepositoryPath
        if TortoiseProc:
            Self._TortoiceProc = TortoiseProc

        Self._Commands = Self.BuildCommands()

        commands = [v[0] for v in Self._Commands]
        Self.window.show_quick_panel(commands, Self._onCommandChoosen)