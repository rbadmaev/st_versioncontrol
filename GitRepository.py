from TortoiseRepository import TortoiseRepositoryCommand

class GitRepositoryCommand(TortoiseRepositoryCommand):

    def __init__(Self, Window, RepositoryPath = ""):
        super().__init__(Window, RepositoryPath)

    def Name(Self):
        return "GIT"

    def BuildCommands(Self):
        return [[Self.Name() + ": Show log...",             ["TortoiseGitProc", "/command:log",             "/path:" + Self._PathPlaceholder], False, False, []],
                [Self.Name() + ": Show modifications...",   ["TortoiseGitProc", "/command:repostatus",      "/path:" + Self._PathPlaceholder], False, False, []],
                [Self.Name() + ": Commit...",               ["TortoiseGitProc", "/command:commit",          "/path:" + Self._PathPlaceholder], False, False, []],
                [Self.Name() + ": Merge...",                ["TortoiseGitProc", "/command:merge",           "/path:" + Self._PathPlaceholder], False, False, []],
                # [Self.Name() + ": Repository: SVN dcommit", ["git", "svn",    "dcommit"],                                                      False, False, ["Do you realy want to commit all your changes to SVN server?", "Yes"]],
                # [Self.Name() + ": Repository: SVN rebase",  ["git", "svn",    "rebase"],                                                       False, False, ["Do you realy want to update from SVN server and rebase local commit?", "Yes"]],
                [Self.Name() + ": Revert changes...",       ["TortoiseGitProc", "/command:revert",          "/path:" + Self._PathPlaceholder], False, False, []],
                [Self.Name() + ": Repository: Stash...",    ["TortoiseGitProc", "/command:stashsave",       "/path:" + Self._PathPlaceholder], False, False, []],
                [Self.Name() + ": Repository: Stash apply...", ["TortoiseGitProc", "/command:stashapply",   "/path:" + Self._PathPlaceholder], False, False, []],
                [Self.Name() + ": Repository: Cleanup",     ["TortoiseGitProc", "/command:cleanup",         "/path:" + Self._PathPlaceholder], False, True,  []],
                [Self.Name() + ": File: Diff",              ["TortoiseGitProc", "/command:diff",            "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": File: Blame",             ["TortoiseGitProc", "/command:blame",           "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": File: Add",               ["TortoiseGitProc", "/command:add",             "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": File: Remove",            ["TortoiseGitProc", "/command:remove",          "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": File: Rename",            ["TortoiseGitProc", "/command:rename",          "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": File: Resolve",           ["TortoiseGitProc", "/command:resolve",         "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": File: Edit conflict",     ["TortoiseGitProc", "/command:conflicteditor",  "/path:" + Self._PathPlaceholder], True,  False, []],
                [Self.Name() + ": Settings",                ["TortoiseGitProc", "/command:settings"],                                          False, False, []]]