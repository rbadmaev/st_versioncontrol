from TortoiseRepository import TortoiseRepositoryCommand

class SvnRepositoryCommand(TortoiseRepositoryCommand):

    def __init__(Self, Window, RepositoryPath = ""):
        super().__init__(Window, RepositoryPath)

    def Name(Self):
        return "SVN"

    def BuildCommands(Self):
        return [[Self.Name() + ": Show log...",             ["TortoiseProc", "/command:log",        "/path:" + Self._PathPlaceholder], False,    False, []],
                [Self.Name() + ": Show modifications...",   ["TortoiseProc", "/command:repostatus", "/path:" + Self._PathPlaceholder], False,    False, []],
                [Self.Name() + ": Update...",               ["TortoiseProc", "/command:update",     "/path:" + Self._PathPlaceholder], False,    False, []],
                [Self.Name() + ": Commit...",               ["TortoiseProc", "/command:commit",     "/path:" + Self._PathPlaceholder], False,    False, []],
                [Self.Name() + ": Revert changes...",       ["TortoiseProc", "/command:revert",     "/path:" + Self._PathPlaceholder], False,    False, []],
                [Self.Name() + ": Repository: Cleanup",     ["TortoiseProc", "/command:cleanup",    "/path:" + Self._PathPlaceholder], False,    True,  []],
                [Self.Name() + ": File: Show log",          ["TortoiseProc", "/command:log",        "/path:" + Self._PathPlaceholder], True,     False, []],
                [Self.Name() + ": File: Diff",              ["TortoiseProc", "/command:diff",       "/path:" + Self._PathPlaceholder], True,     False, []],
                [Self.Name() + ": File: Blame",             ["TortoiseProc", "/command:blame",      "/path:" + Self._PathPlaceholder], True,     False, []],
                [Self.Name() + ": File: Revert",            ["TortoiseProc", "/command:revert",     "/path:" + Self._PathPlaceholder], True,     False, []],
                [Self.Name() + ": File: Add",               ["TortoiseProc", "/command:add",        "/path:" + Self._PathPlaceholder], True,     False, []],
                [Self.Name() + ": File: Remove",            ["TortoiseProc", "/command:remove",     "/path:" + Self._PathPlaceholder], True,     False, []],
                [Self.Name() + ": File: Rename",            ["TortoiseProc", "/command:rename",     "/path:" + Self._PathPlaceholder], True,     False, []]]