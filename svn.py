import sublime, sublime_plugin
import os, glob, subprocess

class ShowSvnCommand(sublime_plugin.ApplicationCommand):
    _paths = []
    _Command = ""
    _Commands = []

    def onDone(Self, Index):
        if Index == -1:
            return
        subprocess.Popen(["TortoiseProc", "/command:"+Self._command, "/path:"+Self._paths[Index]])

    def execute(Self, Command, current_file = False, repository = False):
        if repository and current_file:
            sublime.error_message("It can't be chosen both current_file and repository options")
            return

        Self._Command = Command
        Self._paths = []
        path = sublime.active_window().active_view().file_name()
        while glob.glob(path + "\\" + ".svn") == []:
            newpath = os.path.dirname(path)
            if newpath == path:
                break
            if not repository:
                Self._paths += [path]
            if current_file:
                break;
            path = newpath

        if glob.glob(path + "\\" + ".svn") != []:
            Self._paths += [path]

        if len(Self._paths) == 0:
            sublime.error_message("target path not found")
            return

        if len(Self._paths) == 1:
            Self.onDone(0)
            return

        paths = [os.path.basename(v) for v in Self._paths]
        sublime.active_window().show_quick_panel(paths, Self.onDone)

    def onCommandChoosen(Self, Index):
        if Index == -1:
            return
        Self.execute(  Self._Commands[Index][1],
                            Self._Commands[Index][2],
                            Self._Commands[Index][3])

    def OnDone1(Self, Text):
        sublime.error_message(Text)

    def DetermineVersionControlSystem(Self):
        svnPath  = ""
        hgPath    = ""
        gitPath  = ""
        path = sublime.active_window().active_view().file_name()
        while True:
            if glob.glob(path + "\\.svn") != []:
                if
            newpath = os.path.dirname(path)
            if newpath == path:
                break
            if not repository:
                Self._paths += [path]
            if current_file:
                break;
            path = newpath

    def run(Self, fast_mode = False):
        if fast_mode:
            Self._Commands = [["SVN Repository: Show log", "log", False, True],
                                    ["SVN Repository: Show modifications", "repostatus", False, True],
                                    ["SVN Repository: Update", "update", False, True],
                                    ["SVN Repository: Commit", "commit", False, True],
                                    ["SVN Repository: Revert all changes", "revert", False, True],
                                    ["SVN Repository: Cleanup", "cleanup", False, True],
                                    ["SVN File: Show log", "log", True, False],
                                    ["SVN File: Diff", "diff", True, False],
                                    ["SVN File: Blame", "blame", True, False],
                                    ["SVN File: Update", "update", True, False],
                                    ["SVN File: Commit", "commit", True, False],
                                    ["SVN File: Revert", "revert", True, False],
                                    ["SVN File: Add", "add", True, False],
                                    ["SVN File: Remove", "remove", True, False],
                                    ["SVN File: Rename", "rename", True, False]]
        else:
            Self._Commands = [["SVN: Show log...", "log", False, False],
                                    ["SVN: Show modifications...", "repostatus", False, False],
                                    ["SVN: Update...", "update", False, False],
                                    ["SVN: Commit...", "commit", False, False],
                                    ["SVN: Revert changes...", "revert", False, False],
                                    ["SVN: Cleanup...", "cleanup", False, False],
                                    ["SVN File: Diff", "diff", True, False],
                                    ["SVN File: Blame", "blame", True, False],
                                    ["SVN File: Add", "add", True, False],
                                    ["SVN File: Remove", "remove", True, False],
                                    ["SVN File: Rename", "rename", True, False]]

        commands = [v[0] for v in Self._Commands]
        sublime.active_window().show_quick_panel(commands, Self.onCommandChoosen)