import sublime, os
from glob import glob
from sublime_plugin import WindowCommand
from SvnRepository import SvnRepositoryCommand
from GitRepository import GitRepositoryCommand

class VersionControlCommand(WindowCommand):

    def __init__(Self, Window):
        super().__init__(Window)
        Self._Repositories = []

    def _DetermineVersionControlSystem(Self):
        repositories = []
        path = Self.window.active_view().file_name()
        while True:
            if glob(path + "\\.svn") != []:
                repositories += [SvnRepositoryCommand(Self.window, path)]
            # if glob(path + "\\.hg") != []:
            #     repositories += [SvnRepositoryCommand(Self.window, path)]
            if glob(path + "\\.git") != []:
                repositories += [GitRepositoryCommand(Self.window, path)]

            newpath = os.path.dirname(path)
            if newpath == path:
                break
            path = newpath
        return repositories

    def _OnRepositoryChosen(Self, Index):
        if Index == -1:
            return
        Self._Repositories[Index].run()

    def run(Self):
        Self._Repositories = Self._DetermineVersionControlSystem()

        if len(Self._Repositories) == 0:
            sublime.error_message("Repository not found")
            return

        if len(Self._Repositories) == 1:
            Self._Repositories[0].run()
            return

        Self.window.show_quick_panel(
            [v.Name() + ": " + v.RepositoryPath for v in Self._Repositories],
            Self._OnRepositoryChosen)